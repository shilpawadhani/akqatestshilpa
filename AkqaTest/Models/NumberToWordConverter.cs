﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace AkqaTest.Models
{
    public class NumberToWordConverter
    {
        public string Name { get; set; }
        public string Number { get; set; }

        /// <summary>
        /// Check if fields are valid
        /// </summary>
        /// <returns></returns>
        public bool IsValid()
        {
            if(!String.IsNullOrEmpty(Name) && !String.IsNullOrEmpty(Number))
            {
                if (Regex.IsMatch(Number, @"^[0-9]+(\.[0-9]+)?$"))
                {
                    return true;
                }
                return false;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Convert one place value to words
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private String OnesToWords(String number)
        {
            int num = Convert.ToInt32(number);
            String name = "";
            switch (num)
            {

                case 1:
                    name = "One";
                    break;
                case 2:
                    name = "Two";
                    break;
                case 3:
                    name = "Three";
                    break;
                case 4:
                    name = "Four";
                    break;
                case 5:
                    name = "Five";
                    break;
                case 6:
                    name = "Six";
                    break;
                case 7:
                    name = "Seven";
                    break;
                case 8:
                    name = "Eight";
                    break;
                case 9:
                    name = "Nine";
                    break;
            }
            return name;
        }

        /// <summary>
        /// Convert tens place value to words
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        private String TensToWords(String number)
        {
            int num = Convert.ToInt32(number);
            String name = "";
            switch (num)
            {
                case 10:
                    name = "Ten";
                    break;
                case 11:
                    name = "Eleven";
                    break;
                case 12:
                    name = "Twelve";
                    break;
                case 13:
                    name = "Thirteen";
                    break;
                case 14:
                    name = "Fourteen";
                    break;
                case 15:
                    name = "Fifteen";
                    break;
                case 16:
                    name = "Sixteen";
                    break;
                case 17:
                    name = "Seventeen";
                    break;
                case 18:
                    name = "Eighteen";
                    break;
                case 19:
                    name = "Nineteen";
                    break;
                case 20:
                    name = "Twenty";
                    break;
                case 30:
                    name = "Thirty";
                    break;
                case 40:
                    name = "Fourty";
                    break;
                case 50:
                    name = "Fifty";
                    break;
                case 60:
                    name = "Sixty";
                    break;
                case 70:
                    name = "Seventy";
                    break;
                case 80:
                    name = "Eighty";
                    break;
                case 90:
                    name = "Ninety";
                    break;
                default:
                    if (num > 0)
                    {
                        name = TensToWords(number.Substring(0, 1) + "0") + " " + OnesToWords(number.Substring(1));
                    }
                    break;
            }
            return name;
        }

        /// <summary>
        /// Convert to whole number to words
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public string WholeNumberToWords(string number)
        {
            string numwords = "";
            try
            {
                bool isCompleted = false; 
                while (number.StartsWith("0"))
                {

                    if (number.Length > 1)
                    {
                        number = number.Substring(1);
                    }
                    else
                    {
                        isCompleted = true;
                        break;
                    }

                }
                long numValue = (Convert.ToInt64(number));
                if (numValue > 0)
                {


                    int numDigits = number.Length;
                    int pos = 0;//store digit grouping  
                    String place = "";//digit grouping name:hundres,thousand,etc...  
                    switch (numDigits)
                    {
                        case 0:
                            isCompleted = true;
                            break;
                        case 1://ones' range
                            numwords = OnesToWords(number);
                            isCompleted = true;
                            break;
                        case 2://tens' range  
                            numwords = TensToWords(number);
                            isCompleted = true;
                            break;
                        case 3://hundreds' range  
                            pos = (numDigits % 3) + 1;
                            place = " Hundred ";
                            break;
                        case 4://thousands' range  
                        case 5:
                        case 6:
                            pos = (numDigits % 4) + 1;
                            place = " Thousand ";
                            break;
                        case 7://millions' range  
                        case 8:
                        case 9:
                            pos = (numDigits % 7) + 1;
                            place = " Million ";
                            break;
                        default://billions's range
                            pos = (numDigits % 10) + 1;
                            place = " Billion ";
                            break;
                    }
                    if (!isCompleted)
                    {                         
                        try
                        {
                            numwords = WholeNumberToWords(number.Substring(0, pos)) + place + WholeNumberToWords(number.Substring(pos));
                        }
                        catch { }
                    }
                    if (numwords.Trim().Equals(place.Trim())) numwords = "";
                }
            }
            catch { }
            return numwords.Trim();
        }

        /// <summary>
        /// Convert decimal number or any number to words
        /// </summary>
        /// <returns></returns>
        public string ConvertToWords()
        {
            String val = "", wholeNumber = Number, points = "", StringAnd = "", StringPoint = "";
            String StringEnd = "";
            try
            {
                int decimalPlace = Number.IndexOf(".");
                if (decimalPlace > 0)
                {
                    wholeNumber = Number.Substring(0, decimalPlace);
                    points = Number.Substring(decimalPlace + 1);
                    if (points.Length > 2)
                    {
                        points = points.Substring(0, 2);
                    }
                    else if (points.Length == 1)
                    {
                        points = points + "0";
                    }
                    if (Convert.ToInt32(points) > 0)
                    {
                        StringAnd = "And ";
                        StringEnd = "Cents";
                        StringPoint = WholeNumberToWords(points);
                    }
                }
                wholeNumber = WholeNumberToWords(wholeNumber).Trim();
                if (string.IsNullOrEmpty(wholeNumber)) 
                {
                    wholeNumber = "Zero";
                }
                wholeNumber = wholeNumber + " Dollar";
                val = String.Format("{0} {1}{2} {3}", wholeNumber, StringAnd, StringPoint, StringEnd);
            }
            catch { }
            return val;
        }

    }
}
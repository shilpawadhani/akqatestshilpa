﻿using AkqaTest.Models;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AkqaTest.Controllers
{
    public class ConverterController : ApiController
    {
        [Route("convert/number")]
        [HttpPost]
        public HttpResponseMessage ConvertNumberToWord(NumberToWordConverter nm)
        {
            if (nm.IsValid())
            {
                string words = nm.ConvertToWords();
                if (!String.IsNullOrEmpty(words))
                {
                    return Request.CreateResponse(HttpStatusCode.OK, words, Configuration.Formatters.JsonFormatter);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, "Something went wrong.");
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest,"Please provide valid values.");
            }
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AkqaTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AkqaTest.Models.Tests
{
    [TestClass()]
    public class NumberToWordConverterTests
    {
        NumberToWordConverter numberToWordConverter = new NumberToWordConverter();

        public void IsValidTest1()
        {
            numberToWordConverter.Number = "";
            numberToWordConverter.Name = "";
            Assert.IsFalse(numberToWordConverter.IsValid());
        }
        [TestMethod()]
        public void IsValidTest2()
        {
            numberToWordConverter.Number = "67537";
            numberToWordConverter.Name = "";
            Assert.IsFalse(numberToWordConverter.IsValid());
        }
        [TestMethod()]
        public void IsValidTest3()
        {
            numberToWordConverter.Number = "7377.98";
            numberToWordConverter.Name = "Shilpa";
            Assert.IsTrue(numberToWordConverter.IsValid());
        }
        [TestMethod()]
        public void IsValidTest4()
        {
            numberToWordConverter.Number = "78kr";
            numberToWordConverter.Name = "Shilpa";
            Assert.IsFalse(numberToWordConverter.IsValid());
        }
        [TestMethod()]
        public void IsValidTest5()
        {
            numberToWordConverter.Number = "00099828488382";
            numberToWordConverter.Name = "Shilpa";
            Assert.IsTrue(numberToWordConverter.IsValid());
        }

        [TestMethod()]
        public void NumberToWordConverterTest1()
        {
            numberToWordConverter.Number = "000932.74";
            string words = "Nine Hundred Thirty Two Dollar And Seventy Four Cents";
            string response = numberToWordConverter.ConvertToWords();
            Assert.AreEqual(words, response);
        }
        [TestMethod()]
        public void NumberToWordConverterTest2()
        {
            numberToWordConverter.Number = "30075.65";
            string words = "Thirty Thousand Seventy Five Dollar And Sixty Five Cents";
            string response = numberToWordConverter.ConvertToWords();
            Assert.AreEqual(words, response);
        }
        [TestMethod()]
        public void NumberToWordConverterTest3()
        {
            numberToWordConverter.Number = "704.03";
            string words = "Seven Hundred Four Dollar And Three Cents";
            string response = numberToWordConverter.ConvertToWords();
            Assert.AreEqual(words, response);
        }
        [TestMethod()]
        public void NumberToWordConverterTest4()
        {
            numberToWordConverter.Number = "0.0";
            string words = "Zero Dollar";
            string response = numberToWordConverter.ConvertToWords();
            Assert.AreEqual(words, response.Trim());
        }

        [TestMethod()]
        public void NumberToWordConverterTest5()
        {
            numberToWordConverter.Number = "-99";
            string words = "Zero Dollar";
            string response = numberToWordConverter.ConvertToWords();
            Assert.AreEqual(words, response.Trim());
        }

        [TestMethod()]
        public void WholeNumberConverterTest1()
        {
            string number = "991320099";
            string words = "Nine Hundred Ninety One Million Three Hundred Twenty Thousand Ninety Nine";
            string response = numberToWordConverter.WholeNumberToWords(number);
            Assert.AreEqual(words, response);
        }

        [TestMethod()]
        public void WholeNumberConverterTest2()
        {
            string number = "00056";
            string words = "Fifty Six";
            string response = numberToWordConverter.WholeNumberToWords(number);
            Assert.AreEqual(words, response);
        }
      
    }
}